-module(lake_SUITE).

-export([all/0,
         init_per_group/2,
         end_per_group/2,
         groups/0]).
-export([connect_and_close/1,
         tls_connect_and_close/1,
         connect_incorrect_credentials/1,
         connect_incorrect_vhost/1,
         subscribe_and_publish/1,
         subscribe_and_publish_single_active_consumer/1,
         credit/1,
         credit_wrong_subscription_id/1,
         query_publisher_sequence/1,
         query_publisher_sequence_errors/1,
         store_and_query_offset/1,
         metadata/1,
         delete_without_create/1,
         metadata_update/1,
         close/1,
         heartbeat/1,
         async_publish/1,
         route/1,
         route_unknown_stream/1,
         partitions/1,
         stream_stats/1]).

-include_lib("common_test/include/ct.hrl").
-include_lib("amqp_client/include/amqp_client.hrl").

-include("response_codes.hrl").

all() ->
    [connect_and_close,
     tls_connect_and_close,
     connect_incorrect_credentials,
     connect_incorrect_vhost,
     subscribe_and_publish,
     {group, disable_exchange_command_versions},
     credit,
     credit_wrong_subscription_id,
     query_publisher_sequence,
     query_publisher_sequence_errors,
     store_and_query_offset,
     metadata,
     delete_without_create,
     metadata_update,
     close,
     heartbeat,
     async_publish,
     {group, with_superstream},
     stream_stats].

groups() ->
    [{with_superstream, [], [route, route_unknown_stream, partitions]},
     {disable_exchange_command_versions, [], [connect_and_close, subscribe_and_publish]}].

init_per_group(with_superstream, Config) ->
    %% Set up a SuperStream. A SuperStream cannot be declared using the binary protocol, so we use AMQP.
    SuperStream = superstream(),
    RoutingKeys = superstream_routing_keys(),
    {ok, Connection} = amqp_connection:start(#amqp_params_network{host = host()}),
    {ok, Channel} = amqp_connection:open_channel(Connection),
    ok = superstream:declare(Channel, SuperStream),
    [ok = superstream:declare_and_bind_partition(Channel, SuperStream, RoutingKey)
     || RoutingKey <- RoutingKeys],
    ok = amqp_channel:close(Channel),
    ok = amqp_connection:close(Connection),
    Config;
init_per_group(disable_exchange_command_versions, Config) ->
    %% Add {exchange_command_versions, false} as an additional connection option
    [{connect_options, [{exchange_command_versions, false}]} | Config];
init_per_group(_, Config) ->
    Config.

end_per_group(with_superstream, Config) ->
    SuperStream = superstream(),
    RoutingKeys = superstream_routing_keys(),
    {ok, Connection} = amqp_connection:start(#amqp_params_network{host = host()}),
    {ok, Channel} = amqp_connection:open_channel(Connection),
    [ok = superstream:delete_partition(Channel, SuperStream, RoutingKey) || RoutingKey <- RoutingKeys],
    ok = superstream:delete(Channel, SuperStream),
    ok = amqp_channel:close(Channel),
    ok = amqp_connection:close(Connection),
    Config;
end_per_group(_, Config) ->
    Config.

host() ->
    case os:getenv("RABBITMQ_HOST") of
        false ->
            throw("RABBITMQ_HOST not set");
        Host ->
            Host
    end.

port() ->
    5552.

tls_port() ->
    5551.

stream() ->
    <<"test-stream">>.

superstream() ->
    <<"test-superstream">>.

superstream_routing_keys() ->
    [<<"key1">>, <<"key2">>].

connect_and_close(Config) ->
    Options = proplists:get_value(connect_options, Config, []),
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>, Options),
    ok = lake:stop(Connection),
    ok.

tls_connect_and_close(_Config) ->
    {ok, Connection} = lake:tls_connect(host(), tls_port(), <<"guest">>, <<"guest">>, <<"/">>),
    ok = lake:stop(Connection),
    ok.

connect_incorrect_credentials(_Config) ->
    {error, _} = lake:connect(host(), port(), <<"does not exit">>, <<"guest">>, <<"/">>),
    {error, _} = lake:connect(host(), port(), <<"guest">>, <<"wrong_password">>, <<"/">>).

connect_incorrect_vhost(_Config) ->
    {error, _} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"does not exist">>).

subscribe_and_publish(Config) ->
    Options = proplists:get_value(connect_options, Config, []),
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>, Options),
    Stream = stream(),
    ok = lake:create(Connection, Stream, []),
    SubscriptionId = 1,
    ok = lake:subscribe(Connection, Stream, SubscriptionId, first, 10, [{<<"some">>, <<"property">>}]),
    PublisherId = 1,
    PublisherReference = <<"my-publisher">>,
    ok = lake:declare_publisher(Connection, Stream, PublisherId, PublisherReference),
    Message = <<"Hello, world!">>,
    [{1, ok}] = lake:publish_sync(Connection, PublisherId, [{1, Message}]),
    OsirisChunk = await_osiris_chunk(),
    {ok, {[Message], Info}} = lake:chunk_to_messages(OsirisChunk),
    #{chunk_id := 0,
      number_of_entries := 1,
      number_of_records := 1} =
        Info,
    ok = lake:unsubscribe(Connection, SubscriptionId),
    ok = lake:delete_publisher(Connection, PublisherId),
    ok = lake:delete(Connection, Stream),
    ok = lake:stop(Connection),
    ok.

subscribe_and_publish_single_active_consumer(_Config) ->
    Properties = [{<<"single-active-consumer">>, <<"true">>}, {<<"name">>, <<"my consumer">>}],
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    Stream = stream(),
    ok = lake:create(Connection, Stream, []),
    SubscriptionId = 1,
    ok = lake:subscribe(Connection, Stream, SubscriptionId, first, 10, Properties),
    {consumer_update, Corr, SubscriptionId, false} = await_message(),
    OffsetSpecification = first,
    ok = lake:consumer_update_response(Connection, Corr, ?RESPONSE_OK, OffsetSpecification),

    PublisherId = 1,
    PublisherReference = <<"my-publisher">>,
    ok = lake:declare_publisher(Connection, Stream, PublisherId, PublisherReference),
    Message = <<"Hello, world!">>,
    [{1, ok}] = lake:publish_sync(Connection, PublisherId, [{1, Message}]),
    OsirisChunk = await_osiris_chunk(),
    {ok, {[Message], Info}} = lake:chunk_to_messages(OsirisChunk),
    #{chunk_id := 0,
      number_of_entries := 1,
      number_of_records := 1} =
        Info,
    ok = lake:unsubscribe(Connection, SubscriptionId),
    ok = lake:delete_publisher(Connection, PublisherId),
    ok = lake:delete(Connection, Stream),
    ok = lake:stop(Connection),
    ok.

credit(_Config) ->
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    Stream = stream(),
    ok = lake:create(Connection, Stream, []),
    SubscriptionId = 1,
    InitialCredits = 0,
    ok = lake:subscribe(Connection, Stream, SubscriptionId, first, InitialCredits, []),
    PublisherId = 1,
    ok = lake:declare_publisher(Connection, Stream, PublisherId, <<"my-publisher">>),
    _ = lake:publish_sync(Connection, PublisherId, [{1, <<"Hello, World">>}]),
    refute_deliver(),
    ok = lake:credit_async(Connection, SubscriptionId, 1),
    OsirisChunk = await_osiris_chunk(),
    {ok, {[_], _}} = lake:chunk_to_messages(OsirisChunk),
    _ = lake:publish_sync(Connection, PublisherId, [{1, <<"Hello, World">>}]),
    refute_deliver(),
    ok = lake:unsubscribe(Connection, SubscriptionId),
    ok = lake:delete_publisher(Connection, SubscriptionId),
    ok = lake:delete(Connection, Stream),

    ok = lake:stop(Connection),
    ok.

credit_wrong_subscription_id(_Config) ->
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    {error, _} = lake:credit_async(Connection, 1000, 1).

query_publisher_sequence(_Config) ->
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    Stream = stream(),
    ok = lake:create(Connection, Stream, []),
    PublisherId = 0,
    PublisherReference = <<"my-publisher">>,
    ok = lake:declare_publisher(Connection, Stream, PublisherId, PublisherReference),
    _ = lake:query_publisher_sequence(Connection, PublisherReference, Stream),
    Message = <<"Hello, world!">>,
    _ = lake:publish_sync(Connection, PublisherId, [{1, Message}]),
    {ok, 1} = lake:query_publisher_sequence(Connection, PublisherReference, Stream),
    _ = lake:publish_sync(Connection, PublisherId, [{2, Message}]),
    {ok, 2} = lake:query_publisher_sequence(Connection, PublisherReference, Stream),
    ok = lake:delete_publisher(Connection, PublisherId),
    ok = lake:delete(Connection, Stream),
    ok = lake:stop(Connection),
    ok.

query_publisher_sequence_errors(_Config) ->
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    Stream = stream(),
    ok = lake:create(Connection, Stream, []),
    PublisherId = 0,
    PublisherReference = <<"my-publisher">>,
    ok = lake:declare_publisher(Connection, Stream, PublisherId, PublisherReference),
    {error, _} = lake:query_publisher_sequence(Connection, PublisherReference, <<"wrong stream">>).

store_and_query_offset(_Config) ->
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    Stream = stream(),
    ok = lake:create(Connection, Stream, []),
    PublisherId = 0,
    PublisherReference = <<"my-publisher">>,
    ok = lake:declare_publisher(Connection, Stream, PublisherId, PublisherReference),
    %% No offset yet - the stream has not been written too yet
    {error, no_offset} = lake:query_offset(Connection, PublisherReference, Stream),
    MessagesWithIds = [{Id, <<"Hello">>} || Id <- lists:seq(1, 10)],
    _ = lake:publish_sync(Connection, PublisherId, MessagesWithIds),
    %% No offset - the offset was not written back
    {error, no_offset} = lake:query_offset(Connection, PublisherReference, Stream),
    ok = lake:store_offset(Connection, PublisherReference, Stream, 5),
    {ok, 5} = lake:query_offset(Connection, PublisherReference, Stream),
    ok = lake:delete_publisher(Connection, PublisherId),
    ok = lake:delete(Connection, Stream),
    ok = lake:stop(Connection),
    ok.

metadata(_Config) ->
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    Stream = stream(),
    ok = lake:create(Connection, Stream, []),
    {ok, _Endpoints, _Metadata} = lake:metadata(Connection, [Stream, <<"does not exist">>]),
    ok = lake:delete(Connection, Stream),
    ok = lake:stop(Connection),
    ok.

delete_without_create(_Config) ->
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    {error, _} = lake:delete(Connection, stream()).

metadata_update(_Config) ->
    %% MetadataUpdate may be triggered if a stream with active subscriptions is deleted
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    Stream = stream(),
    ok = lake:create(Connection, Stream, []),
    ok = lake:subscribe(Connection, Stream, 1, first, 10, []),
    ok = lake:declare_publisher(Connection, Stream, 1, <<"publisher">>),
    ok = lake:delete(Connection, Stream),
    %% Ensure that the deletion above is handled and a metadata update has been sent
    {ok, _Endpoints, _Metadata} = lake:metadata(Connection, [Stream]),
    %% We expect the message `metadata_update' twice; once for the subscriber, once for the publisher
    {metadata_update, ?RESPONSE_STREAM_NOT_AVAILABLE, Stream} = await_message(),
    {metadata_update, ?RESPONSE_STREAM_NOT_AVAILABLE, Stream} = await_message(),
    ok = lake:stop(Connection),
    ok.

close(_Config) ->
    %% Client initiates stop.
    {ok, Connection1} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    ok = lake:stop(Connection1),
    %% Server initiates stop due to malformed frame.
    {ok, Connection2} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    Message = <<"invalid">>,
    Size = byte_size(Message),
    Framed = <<Size:32, Message:Size/binary>>,
    unlink(Connection2),
    Ref = monitor(process, Connection2),
    ok = gen_server:call(Connection2, {debug, forward, Framed}),
    {'DOWN', Ref, _, _, _} = await_message().

heartbeat(_Config) ->
    {ok, Connection} =
        lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>, [{heartbeat, 2}]),
    timer:sleep(4000),
    ok = lake:stop(Connection),
    ok.

async_publish(_Config) ->
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    Stream = stream(),
    ok = lake:create(Connection, Stream, []),
    PublisherId = 1,
    PublisherReference = <<"my-publisher">>,
    ok = lake:declare_publisher(Connection, Stream, PublisherId, PublisherReference),
    Message = <<"Hello, world!">>,
    ok = lake:publish_async(Connection, PublisherId, [{1, Message}]),
    {publish_confirm, _PublisherId, _PublishingIdCount = 1, _PublishingIds = [1]} = await_message(),
    ok = lake:delete_publisher(Connection, PublisherId),
    ok = lake:delete(Connection, Stream),
    ok = lake:stop(Connection),
    ok.

route_unknown_stream(_Config) ->
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    {error, stream_does_not_exist} = lake:route(Connection, <<"my routing key">>, <<"unknown">>),
    ok = lake:stop(Connection).

route(_Config) ->
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    SuperStream = superstream(),
    [RoutingKey | _] = superstream_routing_keys(),
    {ok, [Partition]} = lake:route(Connection, RoutingKey, SuperStream),
    SubscriptionId = 1,
    ok = lake:subscribe(Connection, Partition, SubscriptionId, first, 10, []),
    PublisherId = 1,
    PublisherReference = <<"my-publisher">>,
    ok = lake:declare_publisher(Connection, Partition, PublisherId, PublisherReference),
    Message = <<"Hello, world!">>,
    [{1, ok}] = lake:publish_sync(Connection, PublisherId, [{1, Message}]),
    OsirisChunk = await_osiris_chunk(),
    {ok, {[Message], Info}} = lake:chunk_to_messages(OsirisChunk),
    #{chunk_id := 0,
      number_of_entries := 1,
      number_of_records := 1} =
        Info,
    ok = lake:unsubscribe(Connection, SubscriptionId),
    ok = lake:delete_publisher(Connection, PublisherId),
    ok = lake:stop(Connection),
    ok.

partitions(_Config) ->
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    SuperStream = superstream(),
    {ok, [<<"test-superstream-key1">>, <<"test-superstream-key2">>]} =
        lake:partitions(Connection, SuperStream),
    ok = lake:stop(Connection).

%% FIXME stream_stats for superstreams?
stream_stats(_Config) ->
    {ok, Connection} = lake:connect(host(), port(), <<"guest">>, <<"guest">>, <<"/">>),
    Stream = stream(),
    ok = lake:create(Connection, Stream, []),
    {ok,
     #{<<"committed_chunk_id">> := -1,
       <<"first_chunk_id">> := -1,
       <<"last_chunk_id">> := -1}} =
        lake:stream_stats(Connection, Stream),
    ok = lake:delete(Connection, Stream),
    ok = lake:stop(Connection),
    ok.

await_message() ->
    receive
        M ->
            M
    after 5000 ->
        exit(timeout)
    end.

await_osiris_chunk() ->
    case await_message() of
        {deliver, _SubscriptionId, OsirisChunk} ->
            OsirisChunk;
        {deliver_v2, _SubscriptionId, _CommittedChunkId, OsirisChunk} ->
            OsirisChunk
    end.

refute_deliver() ->
    receive
        {deliver, 1, _} ->
            throw(unexpected);
        {deliver_v2, 1, _, _} ->
            throw(unexpected)
    after 1000 ->
        ok
    end.

%% FIXME test: frame size with large messages
%% FIXME test: after unsubscribe, no more messages are delivered
%% FIXME test: PublishError vs PublishConfirm; do we need to translate error codes?
%% FIXME test: mismatching CRC for chunk_to_messages
%% FIXME test: behaviour if connection is stopped while request is pending
%% FIXME test: QueryPublisherSequence, Credit, StoreOffset,QueryOffset, ...
%% FIXME test: delete publisher if publisher was not declared
%% FIXME test: unsubscribe without prior subscription
