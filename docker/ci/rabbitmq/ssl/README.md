# SSL support

The keys here were generated as described in [RabbitMQ's
documentation](https://www.rabbitmq.com/ssl.html). They are included in the
docker files.

**NOTE**: Obviously, these are not meant to be put into production anywhere!
