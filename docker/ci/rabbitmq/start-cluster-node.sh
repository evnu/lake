#!/bin/bash

set -e

echo 1234 > /var/lib/rabbitmq/.erlang.cookie
chmod 600 /var/lib/rabbitmq/.erlang.cookie
chown rabbitmq:rabbitmq /var/lib/rabbitmq/.erlang.cookie

rabbitmq-server &
sleep 5

if [[ $(hostname) != "rabbitmq-1" ]]; then
    rabbitmqctl stop_app
    rabbitmqctl reset
    rabbitmqctl join_cluster rabbit@rabbitmq-1
    rabbitmqctl start_app
fi

wait
