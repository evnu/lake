# Building the container

```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/evnu/lake/rabbitmq_with_streams .
docker push registry.gitlab.com/evnu/lake/rabbitmq_with_streams
```
