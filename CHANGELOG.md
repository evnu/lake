# Changelog

## 0.2.1

* To support older versions of OTP, convert another `sets:new/1` call to `sets:new/0` for #9.

## 0.2.0

With this version, all (currently) defined messages in [the
specification](https://github.com/rabbitmq/rabbitmq-server/blob/03eb5fc7f5010cd03789d7b0e9a764e38e1f7cf3/deps/rabbitmq_stream/docs/PROTOCOL.adoc)
are implemented. That also means that `SuperStreams` are working. For examples, see
the `lake_SUITE:route/1` and `lake_SUITE:partition/1` tests.

Note that `lake` attempts to exchange command versions when connecting to
RabbitMQ. For older versions of RabbitMQ, this might not be possible (as the
`EXCHANGE_COMMAND_VERSIONS` command might not yet be available). This can be disabled by passing the
option `{exchange_command_versions, false}` to `connect/6`. With that, version
1 of `DELIVER` is also used.

## 0.1.2

* Test if publishing a new version works

## 0.1.1

* Benchmark in the repository
* `lake:publish_async/3`

## 0.1.0

Initial release.
